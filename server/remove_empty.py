# remove boosk with NULL authors and randomize stock
import sqlite3
from random import randint

con = sqlite3.connect('books.db')
con.row_factory = sqlite3.Row
cursor = con.cursor()

def deleteNull():
    sql = 'DELETE FROM books WHERE author = ?'
    cursor.execute(sql, ("",))
    con.commit()

def updatePricesAndStock():
    # convert prices to MXN, 1usd = 19.0 MXN

    cursor.execute("SELECT * FROM books")
    res = cursor.fetchall()

    new_query = "UPDATE Books SET price = ?, stock = ? WHERE isbn = ?"

    for row in res:
        cursor.execute(new_query, (row["price"]*19, randint(0,23), row["isbn"],))
    con.commit()

    print("updated")

# deleteNull()
# updatePricesAndStock()

def setRandomTopSellers():

    con = sqlite3.connect('books.db')
    con.row_factory = sqlite3.Row
    cursor = con.cursor() 

    query = f"SELECT * FROM books WHERE isbn IN (SELECT isbn FROM books ORDER BY RANDOM() LIMIT 100)"
    cursor.execute(query)

    res = cursor.fetchall()

    new_query = "UPDATE Books SET category = ? WHERE isbn = ?"
    for x in res: 
        cursor.execute(new_query, (x["category"]+"-Top Seller", x["isbn"],))
    con.commit()
    print("Categorias agregadas.")

setRandomTopSellers()