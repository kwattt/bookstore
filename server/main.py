from flask import Flask, json, request, jsonify, send_from_directory
from flask_cors import CORS
import sqlite3

app = Flask(__name__, static_folder="")
CORS(app)

RESULT_NUMBER = 30

categories_list = []
categories = {

}

@app.route('/dataset/<path:path>')
def send_img(path):
    return send_from_directory('dataset', path)

@app.route('/api/get_categories', methods=["GET"])
def getCategories():
    return jsonify({"categories": categories_list}), 200

@app.route("/api/get_isbn", methods=["GET"])
def getISBN():
    try: 
        isbn = str(request.args.get("isbn"))
    except:
        return "", 400

    con = sqlite3.connect('books.db')
    con.row_factory = sqlite3.Row
    cursor = con.cursor()

    query = "SELECT * FROM books WHERE isbn = ?"
    cursor.execute(query, (isbn, ))
    book = cursor.fetchone()

    resbook = None
    if book:
        resbook = {
                "name": book["name"],
                "author": book["author"],
                "img": "http://127.0.0.1:5000/" + book["pathj"],
                "date": 000000000,
                "description": "",
                "price": int(book["price"]),
                "tags": book["category"].split("-"),
                "isbn": book["isbn"],
                "stars": book["stars"],
                "stock": book["stock"]
            }
        return jsonify(resbook)

    if not resbook:
        return "", 404

    return jsonify(resbook)

@app.route('/api/get_books', methods=["GET"])
def getBooks():

    try: 
        page = int(request.args.get("page"))
    except:
        return "", 400

    try:
        category = str(request.args.get("category"))
        search = str(request.args.get("search"))
    except:
        category = "None"
        search = "None"

    con = sqlite3.connect('books.db')
    con.row_factory = sqlite3.Row
    cursor = con.cursor() 

    if page == -1:
        query = f"SELECT * FROM books WHERE isbn IN (SELECT isbn FROM books ORDER BY RANDOM() LIMIT {RESULT_NUMBER})"
        cursor.execute(query)
    else:
        if page < 1:
            page = 1

        if category != "None" and len(category) > 2 and search != "None" and len(search) > 2:
            query = "SELECT * FROM books WHERE (category LIKE ?) AND (author LIKE ? OR name LIKE ?) LIMIT ?, ?"
            realCat = f"%{category}%"        
            realSearch = f"%{search}%"        
            cursor.execute(query, (realCat, realSearch, realSearch, RESULT_NUMBER*(page-1), RESULT_NUMBER,))

        elif search != "None" and len(search) > 2:
            query = "SELECT * FROM books WHERE author LIKE ? OR name LIKE ? LIMIT ?, ?"
            realSearch = f"%{search}%"        
            cursor.execute(query, (realSearch, realSearch, RESULT_NUMBER*(page-1), RESULT_NUMBER,))

        elif category != "None":
            query = "SELECT * FROM books WHERE category LIKE ? LIMIT ?, ?"
            realCat = f"%{category}%"        
            cursor.execute(query, (realCat, RESULT_NUMBER*(page-1), RESULT_NUMBER,))

        else: 
            query = "SELECT * FROM books LIMIT ?, ?"
            cursor.execute(query, (RESULT_NUMBER*(page-1), RESULT_NUMBER,))

    res = cursor.fetchall()

    books = []

    for book in res:
        books.append(
            {
                "name": book["name"],
                "author": book["author"],
                "img": "http://127.0.0.1:5000/" + book["pathj"],
                "date": 000000000,
                "description": "",
                "price": int(book["price"]),
                "tags": book["category"].split("-"),
                "isbn": book["isbn"],
                "stars": book["stars"],
                "stock": book["stock"]
            }
        )
    return jsonify({"books": books, "pages": 1}), 200

if __name__ == "__main__":
    con = sqlite3.connect('books.db')
    con.row_factory = sqlite3.Row
    cursor = con.cursor()

    query = "SELECT * FROM books"
    cursor.execute(query)

    res = cursor.fetchall()

    categories["-"] = ""
    categories["Top Seller"] = 0

    for book in res:
        for genre in book["category"].split("-"):
            if genre in categories:
                categories[genre] = categories[genre] + 1 
            else:
                categories[genre] = 1 

    categories_list = list(categories.items())

    app.run(debug=True)
