import React from 'react';
// https://www.kaggle.com/lukaanicin/book-covers-dataset

import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import {ChakraProvider} from "@chakra-ui/react";

import theme from "./Theme"
import Home from "./components/Home";
import BookPage from "./components/BookPage";
import CartProvider from "./context/CartContext";
import Checkout from "./components/Checkout";
import Orders from "./components/Orders";
import OrdersProvider from "./context/OrdersContext";

function App() {
  return (
    <div className="App">
        <ChakraProvider theme={theme}>
            <CartProvider>
            <OrdersProvider>
            <Router>
                <Switch>
                    <Route path={"/"} exact>
                        <Home/>
                    </Route>
                    <Route path={"/book/:isbn"}>
                        <BookPage/>
                    </Route>
                    <Route path={"/checkout/"}>
                        <Checkout/>
                    </Route>
                    <Route path={"/orders/"}>
                        <Orders/>
                    </Route>
                    <Route>
                        <>No se encontró :(</>
                    </Route>
                </Switch>
            </Router>
            </OrdersProvider>
            </CartProvider>
        </ChakraProvider>
    </div>
  );
}

export default App;
