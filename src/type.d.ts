interface Book {
    name: string,
    author: string,
    img: string,
    date: Date,
    description: string,
    price: number,
    tags: Array<string>,
    isbn: string | number,
    stars: number,
    stock: number
}

interface CartItem {
    product: Book,
    quantity: number
}

type CartContextState = {
    cart: CartItem[],
    addItem: (book: Book) => void,
    removeItem: (book: Book) => void,
    deleteCart: () => void
}

interface ShipmentInfo {
    name: string,
    lastname: string,
    mail: string,
    phone: string,
    direction: string,
    suburb: string,
    municipy: string,
    state: string,
}

interface PaymentInfo {
    payment: string,
    shipment: string,
    status: string,
    location: string,
}

interface Order {
    id: string,

    direction: ShipmentInfo,
    info: PaymentInfo,

    order: CartItem[]
}

type Orders = {
    orders: Order[]
}

type OrderContextState = {
    orders: Order[],
    addOrder: (order: Order) => void,
}