import React, {createContext, useState, FC, useEffect} from "react";

const OrderContextDefault : OrderContextState = {
    orders: [],
    addOrder: (order: Order) => {}
}

/*
    orders reference __SHOULD NOT BE SAVED IN REAL SCENARIOS__

 */

export const OrdersContext = createContext<OrderContextState>(OrderContextDefault)

const OrdersProvider : FC = ({children}) => {
    const [orders, setOrder] = useState<Order[]>(OrderContextDefault.orders)



    useEffect(() => {
        let storageData = localStorage.getItem("Orders")
        if(typeof storageData !== "object"){
            setOrder(JSON.parse(storageData as string))
        }
    }, [])

    useEffect(()=> {
        localStorage.setItem("Orders", JSON.stringify(orders))
    }, [orders])

    const addOrder = (order: Order) => {
        setOrder((orders) => [...orders, order])
    }

    return (
        <OrdersContext.Provider
            value={{
                orders,
                addOrder,
            }}
        >
            {children}
        </OrdersContext.Provider>
    )
}

export default OrdersProvider