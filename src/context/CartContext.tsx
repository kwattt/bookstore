import React, {createContext, useState, FC, useEffect} from "react";

const CartContextDefault : CartContextState = {
    cart: [],
    addItem: (book: Book) => {},
    removeItem: (book: Book) => {},
    deleteCart: () => {}
}

/*

    book reference __SHOULD NOT BE SAVED IN REAL SCENARIOS__
    This data can be manipulated, we should reference the price from the server storage.

 */

export const CartContext = createContext<CartContextState>(CartContextDefault)

const CartProvider : FC = ({children}) => {
    const [cart, setCart] = useState<CartItem[]>(CartContextDefault.cart)

    useEffect(() => {
        let storageData = localStorage.getItem("Cart")
        if(typeof storageData !== "object"){
            setCart(JSON.parse(storageData as string))
        }
    }, [])

    useEffect(()=> {
        localStorage.setItem("Cart", JSON.stringify(cart))
    }, [cart])

    const addItem = (book: Book) => {

        let newItems : CartItem[] = []
        let found = false

        cart.forEach((val : CartItem) => {
            if(book.isbn === val.product.isbn){
                newItems = [...newItems, {
                    product: book,
                    quantity: val.quantity + 1
                }]
                found = true
            }
            else
                newItems = [...newItems, val]
        })

        if(!found) {
            newItems = [...newItems, {
                product: book,
                quantity: 1
            }]
        }
        setCart(newItems)
    }

    const removeItem = (book: Book) => {
        let newItems : CartItem[] = []
        cart.forEach((val : CartItem) => {
            if(book.isbn === val.product.isbn){
                if(val.quantity !== 1){
                    newItems = [...newItems, {
                        product: book,
                        quantity: val.quantity - 1
                    }]
                }
            }
            else
                newItems = [...newItems, val]
            setCart(newItems)
        })
    }

    const deleteCart = () => {setCart([])}

    return (
        <CartContext.Provider
            value={{
                cart,
                addItem,
                removeItem,
                deleteCart
            }}
        >
            {children}
        </CartContext.Provider>
    )
}

export default CartProvider