import {
    Box,
    Center,
    Heading,
    Image,
    Tag,
    Text
} from "@chakra-ui/react";

import {Link} from "react-router-dom"

const BookView = (book: Book) => {
    return <Link to={"/book/" + book.isbn}><Box
        maxWidth={["60vw", "23vw", "20vw", "12vw"]}
        opacity={0.8}
        _hover={{
            opacity:  "1",
            cursor: "pointer"
        }}
    >
        <Image
            src={book.img}
            w={["60vw", "23vw", "20vw", "12vw"]}
        />

        <Box
            bg={"rgba(255,255,255, 0.2)"}
            borderBottomRadius={7}
        >
            <Heading as={"h3"} size={"sm"}>{book.name}</Heading>
            <Box>
                <b>{book.price}$ - </b>
                <Text display={"inline-block"}>
                    {book.stock !== 0 ?
                        <>Stock: {book.stock}</>
                        :
                        <>Stock: <Text display={"inline-block"} color={"tomato"}>Sin stock</Text></>
                    }
                </Text>

            </Box>
            <Center pb={1}>
                <Box>
                    {book.tags.map((val, id) => {
                        return <Tag m={"1"} key={id} colorScheme={"blue"}>{val}</Tag>
                    })}
                </Box>
            </Center>
        </Box>
    </Box></Link>
}

export default BookView