import axios from "axios";

import {SimpleGrid} from "@chakra-ui/react"
import BookT from "./Books"
import {useState, useEffect} from "react";

type BookRDef = {
    category: string,
    page: number,
    search: string
}

/* TODO:
    Add page buttons.
    Remove search search states and use router for the api calls.
*/

const BookResults = ({category, page, search} : BookRDef)  => {

    const [books, setBooks] = useState<Book[]>([])

    useEffect(() => {
        let _mounted = true
        const fetchData = async() => {
            axios.get("http://127.0.0.1:5000/api/get_books",
                {
                    params: {
                        category: category,
                        page: page,
                        search: search
                    }
                }
            )
            .then((response) => {
                if(_mounted)
                    setBooks(response.data.books)

            })
            .catch(() => {
            })
        }
        fetchData().then()
        return () => {
            _mounted = false
        }
    }, [category, page, search])

    return (
        <SimpleGrid
            ml={"50"}
            minChildWidth={["30vw", "23vw", "20vw", "12vw"]}
            spacing="40px"
            w={"70vw"}
            textAlign={"center"}
        >

        {books.length !== 0 ?
            books.map((val : Book, i : number) => {
                return (
                    <BookT
                        key={i}
                        {...val}
                    />
                )
            })
            :
            <b>No se encontraron libros :(</b>
        }

        </SimpleGrid>
    )
    
}

export default BookResults