import {
    Flex,
    Icon,
    useColorModeValue,
    Heading,
    Text, IconButton,
} from "@chakra-ui/react"
import { Link as ReachLink } from "react-router-dom"
import {BsBook, BsSearch} from "react-icons/bs"

import ShoppingCart from "./ShoppingCart";
import SearchBar from "./SearchBar";

import {
    Link
} from "react-router-dom";


type nav = {
    setSearch?: (val: string) => void
}

const Nav = ({setSearch} : nav) => {
    const bg = useColorModeValue("gray.400", "gray.900")

    return (
        <header>
            <Flex
                as="nav"
                align="center"
                justify="space-between"
                wrap="wrap"
                padding="10px"
                bg={bg}
                color="white"
            >

                <Flex align="center">
                    <Heading as="h3" size="md" mr={"5"}>
                        <Icon as={BsBook}/>
                        <Text as={ReachLink} to="/" ml={5}>{"bookStore"}</Text>
                    </Heading>
                    <ReachLink to={"/orders/"}>Pedidos</ReachLink>
                </Flex>

                <Flex>
                    {typeof setSearch != "undefined" ?
                        <SearchBar setSearch={setSearch}/>
                    :
                        <Link to={"/"}>
                            <IconButton
                                aria-label="Buscar libro"
                                icon={<BsSearch />}
                            />
                        </Link>
                    }
                    <ShoppingCart/>
                </Flex>
            </Flex>
        </header>
    )

}
export default Nav