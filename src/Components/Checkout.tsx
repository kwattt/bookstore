import {
    Box,
    Flex,
    Heading,
    Image, Input,
    Link,
    SimpleGrid,
    Stack,
    Select,
    Text, Button, HStack
} from "@chakra-ui/react";
import Nav from "./Nav";
import Footer from "./Footer";
import {Link as ReachLink} from "react-router-dom";
import {useContext, useState} from "react";
import {CartContext} from "../context/CartContext";
import {OrdersContext} from "../context/OrdersContext";

/*
    TODO:
        This component is slow because of his size, it should be divided on smaller components.
 */

const Checkout = () => {
    const { cart, addItem, removeItem, deleteCart } = useContext(CartContext);

    const [shipmentInfo, setShipmentInfo] = useState<ShipmentInfo>({
        name: "",
        lastname: "",
        mail: "",
        phone: "",
        direction: "",
        suburb: "",
        municipy: "",
        state: ""
    })
    const [paymentInfo, setPaymentInfo] = useState<PaymentInfo>({
        payment: "",
        shipment: "",
        status: "Espera de envío",
        location: "Pendiente"
    })

    const [direction, setDirection] = useState(false)
    const [shipandpayment, setShip] = useState(false)

    const { orders, addOrder } = useContext(OrdersContext);

    let totalPayment = 0

    return         <Box>
        <Nav />
        <SimpleGrid
            mx={"5"}
            mt={"5"}
            minChildWidth={["45vw", "45vw", "25vw", "22vw"]}
            spacing="40px"
            w={"95vw"}
        >
            <Box
                bg={"gray.700"}
                p={"2"}
                borderRadius={"6"}
                contentAlign={"center"}
                textAlign={"center"}
            >
                <Heading>Productos</Heading>
                {cart.length !== 0 ?
                    <Stack
                        spacing={"0"}
                    >
                        {cart.map((item : CartItem) => {
                            totalPayment += item.product.price * item.quantity
                            return <Flex
                                pt={"2"}
                            >
                                    <Image
                                        src={item.product.img}
                                        w={"5vw"}
                                    />
                                    <Box
                                        ml={"5"}
                                        textAlign={"left"}
                                    >
                                        <Link as={ReachLink} to={"/book/" + item.product.isbn}>
                                            <Heading as={"h5"} size={"sm"}>{item.product.name}</Heading>
                                        </Link>
                                        <Text><b>Cantidad:</b> {item.quantity} x {item.product.price}$</Text>
                                        <Text><b>Total:</b> {item.quantity * item.product.price} $</Text>
                                    </Box>
                            </Flex>
                        })}
                        <Box>
                            Total: {totalPayment} $
                        </Box>
                    </Stack>
                    :
                    <b>Tu carrito está vacío :(</b>
                }
            </Box>
            <Box
                bg={"gray.700"}
                p={"2"}
                borderRadius={"6"}
                textAlign={"center"}
            >
                <Heading>Información de envío</Heading>
                <Stack
                    p={"2"}
                    borderRadius={"5"}
                    mt={"5"}
                >

                    <Flex> <b>Nombres: </b>
                        <Input
                            ml={"5"}
                            size={"sm"}
                            onChange={(e) => {setShipmentInfo({...shipmentInfo, name: e.target.value})}}
                        /> </Flex>
                    <Flex> <b>Apellidos: </b>
                        <Input
                            ml={"5"}
                            size={"sm"}
                            onChange={(e) => {setShipmentInfo({...shipmentInfo, lastname: e.target.value})}}
                        /> </Flex>
                    <Flex> <b>Correo: </b>
                        <Input
                            type={"mail"}
                            ml={"5"}
                            size={"sm"}
                            onChange={(e) => {setShipmentInfo({...shipmentInfo, mail: e.target.value})}}
                        /> </Flex>
                    <Flex> <b>Teléfono: </b>
                        <Input
                        ml={"5"}
                        size={"sm"}
                        onChange={(e) => {setShipmentInfo({...shipmentInfo, phone: e.target.value})}}
                        /> </Flex>
                    <Flex> <b>Dirección: </b>
                        <Input
                            ml={"5"}
                            size={"sm"}
                            onChange={(e) => {setShipmentInfo({...shipmentInfo, direction: e.target.value})}}
                        /> </Flex>
                    <Flex> <b>Colonia: </b> <Input
                        ml={"5"}
                        size={"sm"}
                        onChange={(e) => {setShipmentInfo({...shipmentInfo, suburb: e.target.value})}}
                    /> </Flex>
                    <Flex> <b>Municipio: </b>
                        <Input
                            ml={"5"}
                            size={"sm"}
                            onChange={(e) => {setShipmentInfo({...shipmentInfo, municipy: e.target.value})}}
                        /> </Flex>

                    <Flex> <b>Estado: </b>
                        <Select
                            ml={"5"}
                            size={"sm"}
                            onChange={(e) => {setShipmentInfo({...shipmentInfo, state: e.target.value})}}
                        >
                        {MXEstados.map((val, key) => {
                            return <option key={key} value={val}>{val}</option>
                        })}
                        </Select> </Flex>

                </Stack>

                <Button
                    colorScheme={direction ? "red" : "green"}
                    size={"sm"}
                    mt={"3"}
                    onClick={()=> {setDirection(!direction)}}
                >
                    {direction ?  "Cambiar" : "Confirmar"}
                </Button>

            </Box>
            <Box
                bg={"gray.700"}
                p={"2"}
                borderRadius={"6"}
                textAlign={"center"}
            >
                <Heading>Opciones de envío</Heading>
                <Box>
                    <Heading as={"h4"} size={"md"}>Paquetería</Heading>
                    <Select
                        my={"6"}
                        size={"sm"}
                        onChange={(e) => {setPaymentInfo({...paymentInfo, shipment: e.target.value})}}

                    >
                        <option value={"estafeta"}>Estafeta</option>
                        <option value={"fedex"}>Fedex</option>
                        <option value={"dhl"}>DHL</option>
                    </Select>

                    <Heading as={"h4"} size={"md"}>Método de pago</Heading>
                    <Select
                        my={"6"}
                        size={"sm"}
                        onChange={(e) => {setPaymentInfo({...paymentInfo, payment: e.target.value})}}
                    >
                        <option value={"estafeta"}>Paypal</option>
                        <option value={"fedex"}>Tarjeta de débito/crédito</option>
                    </Select>
                </Box>

                <Button
                    colorScheme={shipandpayment ? "red" : "green"}
                    size={"sm"}
                    mt={"3"}
                    onClick={()=> {setShip(!shipandpayment)}}
                >
                    {shipandpayment ?  "Cambiar" : "Confirmar"}
                </Button>

            </Box>

            <Box
                bg={"gray.700"}
                p={"2"}
                borderRadius={"6"}
                textAlign={"center"}
            >
                <Heading>Confirmación</Heading>
                <Box>
                    {(direction && shipandpayment) ?

                        <ReachLink to={"/orders"}>
                        <Button
                            my={"10"}
                            size={"lg"}
                            colorScheme={"green"}
                            onClick={()=> {
                                addOrder({
                                    id: makeid(22),
                                    direction: shipmentInfo,
                                    info: paymentInfo,
                                    order: cart
                                })
                                deleteCart()
                            }}
                        >
                            Pagar
                        </Button>
                        </ReachLink>
                        :
                        <Box
                            mt={"10"}
                        >
                            <b>Necesitas confirmar tus datos.</b>
                        </Box>
                    }
                </Box>
            </Box>
        </SimpleGrid>
        <Footer/>
    </Box>

}

function makeid(length: number) {
    let result           = [];
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }
    return result.join('');
}


const MXEstados = ['Aguascalientes','Baja California','Baja California Sur','Campeche','Chiapas','Chihuahua','Coahuila','Colima','Ciudad de México','Durango','Guanajuato','Guerrero','Hidalgo','Jalisco','Estado de Mexico','Michoacan','Morelos','Nayarit','Nuevo Leon','Oaxaca','Puebla','Queretaro','Quintana Roo','San Luis Potosi','Sinaloa','Sonora','Tabasco','Tamaulipas','Tlaxcala','Veracruz','Yucatan','Zacatecas']

export default Checkout