import {
    Stack,
    Heading,
    Flex,
    Button,
    IconButton,
    Popover,
    PopoverArrow, PopoverBody,
    PopoverCloseButton,
    PopoverContent,
    PopoverHeader,
    PopoverTrigger,
    HStack,
    NumberInput,
    NumberInputField,
    Center,
    Link,
    Box
} from "@chakra-ui/react";

import {Link as ReachLink} from "react-router-dom"

import {useContext} from "react"

import {AiOutlineShoppingCart} from "react-icons/ai";
import {CartContext} from "../context/CartContext";

const ShoppingCart = () => {
    const { cart, addItem, removeItem } = useContext(CartContext);

    let totalPayment = 0

    return <Popover
        closeOnBlur={false}
        autoFocus={false}
    >
        <PopoverTrigger>
            <IconButton
                ml={5}
                aria-label="Carrito de compras"
                icon={<AiOutlineShoppingCart />}
            />
        </PopoverTrigger>
        <PopoverContent>
            <PopoverArrow/>
            <PopoverCloseButton/>
            <PopoverHeader>
                Carrito de compras
            </PopoverHeader>
            <PopoverBody>
                {cart.length !== 0 ?
                    <Stack
                        spacing={"0"}
                    >
                        {cart.map((item : CartItem) => {
                            totalPayment += item.product.price * item.quantity
                            return <>
                                <Flex
                                    justify="space-between"
                                    wrap="wrap"
                                >

                                    <Link as={ReachLink} to={"/book/" + item.product.isbn}>
                                        <Heading as={"h5"} size={"sm"}>{item.product.name}</Heading>
                                    </Link>

                                    <HStack my={"2"}>
                                        <Button
                                            size={"sm"}
                                            colorScheme={"red"}
                                            onClick={() =>{removeItem(item.product)}}
                                        >
                                            {item.quantity > 1 ?
                                                    "-"
                                                    :
                                                    "X"
                                            }
                                        </Button>
                                        <NumberInput
                                            size={"sm"}
                                            defaultValue={item.quantity}
                                            isDisabled
                                            isReadOnly
                                            value={item.quantity}
                                        >
                                            <NumberInputField />
                                        </NumberInput>
                                        <Button
                                            isDisabled={(item.quantity >= 5) || (item.quantity >= item.product.stock)}
                                            colorScheme={"green"}
                                            size={"sm"}
                                            onClick={() => {addItem(item.product)}}
                                        >
                                            +
                                        </Button>
                                        <Box>
                                            {item.product.price * item.quantity}$
                                        </Box>
                                    </HStack>
                                </Flex>
                            </>
                        })}
                        <Box>
                            Total: <b>{totalPayment} $</b>
                        </Box>
                    </Stack>
                    :
                <b>Tu carrito está vacío :(</b>
                }
                <Center>
                    <ReachLink to={"/checkout/"}>
                    <Button
                        mt={"3"}
                       size={"sm"}
                       variant={"outline"}
                       colorScheme={"purple"}
                       borderRadius={0}
                    >
                        Proceder al pago
                    </Button>
                    </ReachLink>
                </Center>
            </PopoverBody>
        </PopoverContent>
    </Popover>
}

export default ShoppingCart