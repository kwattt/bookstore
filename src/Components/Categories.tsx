import { Tabs, TabList, Tab} from "@chakra-ui/react"
import {useEffect, useState} from "react"
import axios from "axios"

type CatType = {
    platform: string,
    setCategory: (v: string) => void
}

const Categories = ({platform, setCategory} : CatType) => {
    const [categories, setCategories] = useState([])

    useEffect(() => {
        let _mounted = true
        const fetchData = async() => {
            axios.get("http://127.0.0.1:5000/api/get_categories")
                .then((response) => {
                    if(_mounted)
                        setCategories(response.data.categories)
                })
                .catch(() => {
                })
        }
        fetchData().then()
        return () => {
            _mounted = false
        }
    }, [])

    if(platform === "pc"){
        return (
            <Tabs
                onChange={()=>{}}
                orientation="vertical"
                marginTop="1vw"
                variant="soft-rounded"
            >
                <TabList>
                    <Contents props={categories} setCategory={setCategory}/>
                </TabList>
            </Tabs>
        )
    }

    else {
        return (
            <Tabs onChange={()=>{}} >
                <TabList>
                    <Contents props={categories} setCategory={setCategory}/>
                </TabList>
            </Tabs>
        )
    }

}

const Contents = ({props, setCategory} : {props: Array<string>, setCategory: (e: string) => void}) => {
    return (<>
        {
            props.map((val : any, id: number) => {
                return (<Tab
                    key={id}
                    value={val[0]}
                    size="sm"
                    onClick={() => {setCategory(val[0])}}
                >
                    {val[0] + " " + val[1]}
                </Tab>)
            })
        }
    </>)
}


export default Categories