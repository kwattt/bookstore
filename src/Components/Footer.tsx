import {
    Center,
    Flex,
    Icon,
    Text
} from "@chakra-ui/react";
import {BsBook} from "react-icons/bs"

const Footer = () => {
    return (<Center
        position={"relative"}
    >
        <Flex>
            <Icon as={BsBook}/>
            <Text ml={2} fontSize="sm" >
                &copy; {new Date().getFullYear()} bookStore. Derechos reservados.
            </Text>
        </Flex>
    </Center>)
}

export default Footer