import {useEffect, useState} from "react";

import {BsSearch} from "react-icons/bs"
import {IconButton, Input} from "@chakra-ui/react";
import {useDebounce} from "use-debounce";

type searchBar = {
    setSearch: (v: string) => void,
}

const SearchBar = ({setSearch} : searchBar) => {
    const [searchOpen, setSearchOpen] = useState(false)
    const [realSearch, setRealSearch] = useState("")
    const [bouncedSearch] = useDebounce(realSearch, 200);

    useEffect(() => {
        setSearch(bouncedSearch)
    }, [bouncedSearch, setSearch])

    return ( <>
        {searchOpen ?
            <Input
                placeholder={"Buscar.."}
                display={"inline"}
                onChange={(e)=>{
                    setRealSearch(e.target.value)
                }}
            />
            :
            <IconButton
                aria-label="Buscar libro"
                icon={<BsSearch />}
                onClick={()=>{setSearchOpen(!searchOpen)}}
            />
        }
    </>)
}

export default SearchBar