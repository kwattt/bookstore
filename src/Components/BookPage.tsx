import {useParams} from 'react-router-dom'

import Nav from "./Nav";
import Footer from "./Footer";

import {Box, Button, Center, Heading, Image, Stack, Tag, Text} from "@chakra-ui/react";
import {useContext, useEffect, useState} from "react";
import axios from "axios";
import {CartContext} from "../context/CartContext";

type searchParams = {
    isbn: string
}

const BookPage = () => {
    const { cart, addItem, removeItem } = useContext(CartContext);

    const {isbn} = useParams<searchParams>();

    const [book, setBook] = useState<Book>()

    useEffect(() => {
        let _mounted = true
        const fetchData = async() => {
            axios.get("http://127.0.0.1:5000/api/get_isbn",
                {
                    params: {
                        isbn: isbn
                    }
                })
                .then((response) => {
                    if(_mounted)
                        setBook(response.data)
                })
                .catch(() => {
                })
        }
        fetchData().then()
        return () => {
            _mounted = false
        }
    }, [isbn])

    return (

        <Box>
            <Nav />
            <Box
                minHeight={"calc(83vh)"}
                m={"10"}
            >
                {typeof book != "undefined" &&
                    <Box
                        bg={"rgba(200,200,200, 0.2)"}
                        borderRadius={"6"}
                        p={"10"}
                        pt={"2"}
                        pb={"2"}
                        position={"absolute"}
                        contentAlign={"center"}
                        textAlign={"center"}
                    >
                        <Heading>{book.name}</Heading>
                        <Heading>{book.author}</Heading>
                        <Center>
                        <Image
                            pt={"2"}
                            minW={["60vw", "23vw", "20vw", "12vw"]}
                            src={book.img}
                        />
                        </Center>

                            <Box
                                mt={"2"}
                            >
                                {book.tags.map((val, id) => {
                                    return <Tag
                                        m={"1"}
                                        key={id}
                                        colorScheme={"blue"}
                                        borderRadius={"0"}
                                    >
                                        {val}
                                    </Tag>
                                })}
                            </Box>
                            <Box
                                textAlign={"left"}
                            >
                                <Heading>Información</Heading>
                                <Stack>
                                    <Box>ISBN: {book.isbn}</Box>
                                    <Box>Título: {book.name}</Box>
                                    <Box>Autor: {book.author}</Box>
                                    <Box>Calificación: {book.stars}/5</Box>
                                </Stack>
                                <Heading>Disponibilidad</Heading>
                                <Stack
                                    mt={"5"}
                                    spacing={"1"}
                                >
                                    <Box>
                                        <b>Precio: </b> {book.price}$ MXN
                                    </Box>
                                    <Box>
                                        <Text display={"inline-block"}>
                                            {book.stock !== 0 ?
                                                <><b>Stock:</b> {book.stock}</>
                                                :
                                                <> <b>Stock:</b> <Text display={"inline-block"} color={"tomato"}>Sin stock</Text></>
                                            }
                                        </Text>
                                    </Box>
                                </Stack>
                            </Box>
                        {book.stock !== 0 &&
                            <Button
                                borderRadius={"sm"}
                                colorScheme={"purple"}
                                onClick={()=> {
                                    addItem(book)
                                }}
                            >
                                Agregar al carrito
                            </Button>
                        }
                    </Box>
                }
            </Box>
            <Footer/>
        </Box>
    )
}

export default  BookPage