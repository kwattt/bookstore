import {Box, Button, Center, Heading, Image, SimpleGrid, Stack, Tag, Text} from "@chakra-ui/react";
import Nav from "./Nav";
import Footer from "./Footer";
import {useContext} from "react";
import {OrdersContext} from "../context/OrdersContext";

const Orders = () => {
    const { orders, addOrder } = useContext(OrdersContext);

    return (
        <Box>
            <Nav />
            <Box
                minHeight={"calc(83vh)"}
                m={"10"}
            >
                <Heading>Pedidos actuales</Heading>

                <SimpleGrid
                    mx={"5"}
                    mt={"5"}
                    minChildWidth={["45vw", "45vw", "25vw", "22vw"]}
                    spacing="40px"
                    w={"95vw"}
                >
                    {orders.map(order => {
                        return <Box
                            p={"3"}
                            bg={"rgba(100,100,100, 0.2)"}
                            borderRadius={"5"}
                        >
                            <Stack>
                                <Box>
                                    <Heading as={"h5"} size={"xs"}>Orden: {order.id}</Heading>
                                </Box>
                                <Box>
                                    <Heading as={"h5"} size={"xs"}>Informacion</Heading>
                                    {order.direction.name} {order.direction.lastname}<br/>
                                    {order.direction.mail}<br/>
                                    {order.direction.phone}<br/>
                                    {order.direction.direction}<br/>
                                    {order.direction.suburb}<br/>
                                    {order.direction.municipy}<br/>
                                    {order.direction.state}<br/>
                                </Box>
                                <Box>
                                    <Heading as={"h5"} size={"xs"}>Carrito</Heading>
                                    {order.order.map(item => {
                                        return <Box>
                                            <b>{item.product.name} </b> {item.quantity} x {item.product.price}
                                        </Box>
                                    })}
                                </Box>
                                <Box>
                                    <Heading as={"h5"} size={"xs"}>Envío</Heading>
                                    <Box>
                                        Estado: {order.info.status}<br/>
                                        Localización: {order.info.location}
                                    </Box>
                                </Box>

                            </Stack>

                        </Box>
                    })}

                </SimpleGrid>
            </Box>
            <Footer/>
        </Box>
    )
}

export default Orders