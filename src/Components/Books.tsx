import {
    Box,
} from "@chakra-ui/react"

import BookView from "./BookView"

const Book = (book : Book) => {
    return (<Box>
        <BookView {...book}/>
    </Box>)
}

export default Book