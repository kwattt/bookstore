
import Nav from "./Nav"
import {Box, SimpleGrid, Center, Heading} from "@chakra-ui/react";
import Footer from "./Footer";
import BookResults from "./BookResults";
import Categories from "./Categories";
import {useState} from "react";


const Home = () => {
    const [search, setSearch] = useState("")
    const [category, setCategory] = useState("")
    const [page, setPage] = useState(1)

    return (<div>
        <Nav setSearch={setSearch}/>
        <Box
            minHeight={"calc(83vh)"}
            mb={"5"}
        >
            <Box
                marginTop={{base:"10px", sm:"25px"}}
            >
                <Box
                    minWidth={{base: "auto", sm: "120px"}}
                    marginX={{base: "auto", sm: "3vw"}}
                >
                    <Box display={{base: "auto", sm: "none"}}> {/*mobile*/}
                        <Center><Heading as="h4" fontSize={"xl"}>Categorías</Heading></Center>
                        <Box overflow="scroll" borderBottom="solid 1px white">
                            <Categories setCategory={setCategory} platform={"mobile"}/>
                        </Box>
                    </Box>

                    <SimpleGrid columns={{base: 0, sm: 6}} rows={{base: 5, sm: 0}} spacing={5} px={{base: "10px", sm:"auto"}}>
                        <Box height={{base: "auto", sm: "80px"}}>
                            <Box display={{base: "none", sm: "block"}} w={300}>  {/*pc*/}
                                <Heading ml={9} as="h4" fontSize={["sm", "md", "lg", "xl"]}>Categorías</Heading>
                                <Categories setCategory={setCategory} platform={"pc"}/>
                            </Box>
                        </Box>

                        <BookResults category={category} page={page} search={search}/>

                    </SimpleGrid>

                </Box>
            </Box>
        </Box>
        <Footer/>
    </div>)
}

export default Home